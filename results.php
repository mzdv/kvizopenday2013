<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Openday 2013 kviz</title>
	<link rel="stylesheet" href="css/results.css">
	<!-- <link href='http://fonts.googleapis.com/css?family=Open+Sans&subset=latin,latin-ext' rel='stylesheet' type='text/css'> -->
</head>
<body>
	<center>
		<img src="img/logo.png" alt="FONIS logo" class="logo">
	</center>
	<div class="quiz">
		<?php

		error_reporting(0);

		$post = $_POST;
		$solutions = array("Axon", "Skraćenica za grafičko okruženje", "Kitkat", "Fallout: New Vegas", "MySQL");
		$score = 0;

		if (isset($post['submit'])) {

			$userInput = array($post['questionOne'], $post['questionTwo'], $post['questionThree'], $post['questionFour'], $post['questionFive']);

			if ((!empty($userInput[0]) && ($userInput[0] === $solutions[0]))) {

				$questionOne = true;
				$score++;
			}
			elseif (empty($userInput[0])) {

				$userInput[0] = "Nije dat.";
				$questionOne = false;
			} else 

			$questionOne = false;

			if ((!empty($userInput[1]) && ($userInput[1] === $solutions[1]))) {

				$questionTwo = true;
				$score++;
			}
			elseif (empty($userInput[1])) {

				$userInput[1] = "Nije dat.";
				$questionTwo = false;
			} else 

			$questionTwo = false;

			if ((!empty($userInput[2]) && ($userInput[2] === $solutions[2]))) {

				$questionThree = true;
				$score++;
			}
			elseif (empty($userInput[2])) {

				$userInput[2] = "Nije dat.";
				$questionThree = false;
			} else 

			$questionThree = false;

			if ((!empty($userInput[3]) && ($userInput[3] === $solutions[3]))) {

				$questionFour = true;
				$score++;
			}
			elseif (empty($userInput[3])) {

				$userInput[3] = "Nije dat.";
				$questionFour = false;

			} else 

			$questionFour = false;

			if ((!empty($userInput[4]) && ($userInput[4] === $solutions[4])))  {

				$questionFive = true;
				$score++;
			}
			elseif (empty($userInput[4])) {

				$userInput[4] = "Nije dat.";
				$questionFive = false;

			} else 

			$questionFive = false;
		}

		for ($i=0; $i < count($userInput); $i++) { 

	// var_dump($userInput[$i]);
			echo "<p>Vaš odgovor: <strong>$userInput[$i]</strong></p>";
			echo "<p>Tačan odgovor: <strong>$solutions[$i]</strong></p>";
		}		

		echo "<h1><center><strong>Rezultat: $score / 5</strong></center></h1>";
		?>
		<center><a href="index.php">Restart</a></center>
	</div>
	