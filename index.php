<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Openday 2013 kviz</title>
	<link rel="stylesheet" href="css/style.css">
	<!-- <link href='http://fonts.googleapis.com/css?family=Open+Sans&subset=latin,latin-ext' rel='stylesheet' type='text/css'> -->
</head>
<body>
	<center>
		<img src="img/logo.png" alt="FONIS logo" class="logo">
	</center>
	<div class="quiz">
		<form action="results.php" method="post" accept-charset="utf-8">
			<div class="questionOne">
				<p>Šta od ovoga nije programski jezik?</p>
				<input type="radio" name="questionOne" id="11" value="Haskell"> Haskell</br>
				<input type="radio" name="questionOne" id="12" value="Java"> Java</br>
				<input type="radio" name="questionOne" id="13" value="Pizza"> Pizza</br>
				<input type="radio" name="questionOne" id="14" value="Axon"> Axon</br>
			</div>
			<div class="questionTwo">
				<p>Šta je GUI?</p>
				<input type="radio" name="questionTwo" id="21" value="Skraćenica za grafičko okruženje"> Skraćenica za grafičko okruženje </br>
				<input type="radio" name="questionTwo" id="22" value="Proizvođač procesora"> Proizvođač procesora </br>
				<input type="radio" name="questionTwo" id="23" value="Antivirusni program"> Antivirusni program </br>
				<input type="radio" name="questionTwo" id="24" value="Ništa od navedenog"> Ništa od navedenog </br>
			</div>
			<div class="questionThree">
				<p>Koje je ime aktuelne verzije operativnog sistema Android?</p>
				<input type="radio" name="questionThree" id="31" value="Gingerbread"> Gingerbread </br>
				<input type="radio" name="questionThree" id="32" value="Key Lime Pie"> Key Lime Pie </br>
				<input type="radio" name="questionThree" id="33" value="Kitkat"> Kitkat </br>
				<input type="radio" name="questionThree" id="34" value="Jellybean"> Jellybean </br>
			</div>
			<div class="questionFour">
				<p>Izbaciti igru koja nije pucačina:</p>
				<input type="radio" name="questionFour" id="41" value="Fallout: New Vegas"> Fallout: New Vegas </br>
				<input type="radio" name="questionFour" id="42" value="Call of Duty: Ghosts"> Call of Duty: Ghosts </br>
				<input type="radio" name="questionFour" id="43" value="ARMA3"> ARMA3 </br>
				<input type="radio" name="questionFour" id="44" value="Battlefield 3"> Battlefield 3 </br>
			</div>
			<div class="questionFive">
				<p>Koja tehnologija nije serverska?</p>
				<input type="radio" name="questionFive" id="51" value="Node.js"> Node.js </br>
				<input type="radio" name="questionFive" id="52" value="PHP"> PHP </br>
				<input type="radio" name="questionFive" id="53" value="MVC4"> MVC4 </br>
				<input type="radio" name="questionFive" id="54" value="MySQL"> MySQL </br>
			</div>
			<button name="submit" type="submit" value="Pošalji" class="submit">Pošalji</button>

		</div>
	</form>
	<script src="js/main.js"></script>
</body>
</html>